/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("open").controller("loginCtrl", function (BASE_PATH, CREDENTIALS, $scope, $window, $location, $http) {
  "use strict";

  var lastRequest = -1;

  /*===================================================== Scope  =====================================================*/

  $scope.error = null;
  $scope.loading = null;

  $scope.passwordMinLength = CREDENTIALS.password.min;
  $scope.credentials = {username: null, password: null};

  $scope.login = login;

  /*------------------------------------------------- Scope Watcher  -------------------------------------------------*/

  $scope.$watch("credentials.username", validateUsername);

  /*=================================================== Functions  ===================================================*/

  /*----------------------------------------------------- Login  -----------------------------------------------------*/

  function login() {
    var reqId = ++lastRequest;
    var data = _.clone($scope.credentials);
    $scope.loading = true;
    $scope.error = null;
    $scope.credentials.password = null;
    $http
        .post("/login", data)
        .then(function () {
          $scope.loading = $scope.error = null;
          $window.location.href = BASE_PATH;
        }, function (err) {
          if (reqId !== lastRequest) { return; }
          if (err.status === 429) { $scope.credentials.password = data.password; }
          $scope.loading = null;
          $scope.error = err;
        });
  }

  /*--------------------------------------------------- Validation ---------------------------------------------------*/

  function validateUsername(value) { $scope.usernameValid = CREDENTIALS.username.regex.test(value || ""); }

});
