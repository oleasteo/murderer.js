/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("closed").factory("closedGames", function (socket, closedModals, games) {
  "use strict";

  /*==================================================== Exports  ====================================================*/

  //noinspection UnnecessaryLocalVariableJS
  var service = {
    join: join,
    leave: leave,
    suicide: suicide,
    kill: kill
  };

  /*=================================================== Functions  ===================================================*/

  function kill(game, ringId, ringIdx) { // TODO indicate processing status, prevent concurrent attempts
    return closedModals
        .qKill(game, ringIdx)
        .then(function (data) {
          return socket.query("murder:token", _.extend(data, {gameId: game._id, ringId: ringId}));
        });
  }

  function join(game) { // TODO indicate processing status, prevent concurrent requests
    return closedModals
        .qJoinGame(game)
        .then(function (data) { return socket.query("game:join", _.extend(data, {gameId: game._id})); })
        .then(function (groups) {
          updateGameParticipation(game, groups);
          games.joined().then(function (gameIds) { gameIds.push(game._id); });
          game.mayJoin = false;
          game.joined = game.mayLeave = true;
          return game;
        }); // TODO proper error handling
  }

  function leave(game) { // TODO indicate processing status, prevent concurrent requests
    return socket
        .query("game:leave", game._id)
        .then(function (groups) {
          updateGameParticipation(game, groups);
          games.joined().then(function (gameIds) {
            var idx = _.indexOf(gameIds, game._id);
            if (~idx) { gameIds.splice(idx, 1); }
          });
          game.mayJoin = true;
          game.joined = game.mayLeave = false;
          return game;
        }); // TODO proper error handling
  }

  function suicide(game) { // TODO indicate processing status, prevent concurrent requests
    return closedModals
        .qSuicide(game)
        .then(function (data) { return socket.query("murder:self", _.extend(data, {gameId: game._id})); })
        .then(function () { game.maySuicide = game.isAlive = false; }); // TODO proper error handling
  }

  function updateGameParticipation(game, groups) {
    game.participants = _.sum(_.map(game.groups = groups, function (g) { return g.users.length; }));
    games.addGroupTitles(game);
  }

  /*===================================================== Return =====================================================*/

  return service;
});
