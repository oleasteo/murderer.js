/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("closed").controller("gamesCtrl", function ($scope, games, closedGames) {
  "use strict";

  /*===================================================== Scope  =====================================================*/

  $scope.loading = true;
  $scope.games = [];
  $scope.displayedGames = [];
  $scope.stateIcon = games.stateIcon;

  $scope.sort = {
    state: games.sortValue.state,
    participants: games.sortValue.participants
  };
  $scope.join = function (game) { closedGames.join(game); };
  $scope.leave = function (game) { closedGames.leave(game); };
  $scope.suicide = function (game) { closedGames.suicide(game); };

  /*=============================================== Initial Execution  ===============================================*/

  games
      .all()
      .then(function (gameArray) {
        Array.prototype.push.apply($scope.games, _.each(gameArray, games.prepareGameListView));
        $scope.loading = false;
      }); // TODO proper error handling

});
