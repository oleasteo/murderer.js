/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("closed").controller("contractsCtrl", function ($scope, $rootScope, socket) {
  "use strict";

  /*===================================================== Scope  =====================================================*/

  $scope.games = null;
  $scope.allGames = null;
  $scope.gameFilter = null;

  $scope.collapsed = true;
  $scope.compact = true;
  $scope.contractsOnly = false;
  $scope.activeOnly = false;

  $scope.replaceContract = function (game, index, contract) { game.contracts[index] = contract; };

  $scope.$watchGroup(["allGames", "gameFilter"], function () {
    $scope.games = $scope.gameFilter ? [_.find($scope.allGames, {_id: $scope.gameFilter})] : $scope.allGames;
  });

  /*=============================================== Initial Execution  ===============================================*/

  socket.query("contracts:active").then(function (games) { $scope.allGames = games; });

});
