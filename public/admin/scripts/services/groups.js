/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("admin").factory("groups", function (socket) {
  "use strict";

  /*==================================================== Exports  ====================================================*/

  //noinspection UnnecessaryLocalVariableJS
  var service = {
    all: function () { return socket.query("groups:all"); },
    create: function (data) { return socket.query("group:create", data); },
    update: function (data) { return socket.query("group:update", data); },
    remove: function (id) { return socket.query("group:remove", id); }
  };

  /*===================================================== Return =====================================================*/

  return service;
});
