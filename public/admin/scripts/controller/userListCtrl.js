/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("admin").controller("userListCtrl", function ($scope, $rootScope, users) {
  "use strict";

  /*===================================================== Scope  =====================================================*/

  $scope.displayedUsers = [];

  $scope.toggleAdmin = toggleAdmin;
  $scope.removeUser = removeUser;
  $scope.isSelf = function (user) { return $rootScope.identity && user._id === $rootScope.identity._id; };

  /*=================================================== Functions  ===================================================*/

  function removeUser(user) {
    user.saving = true;
    users
        .remove(user._id)
        .then(function () {
          var idx = _.indexOf($scope.users, user);
          if (~idx) { $scope.users.splice(idx, 1); }
        }, function (err) {
          console.error(err);
          delete user.saving;
        });
  }

  function toggleAdmin(user) {
    var admin = !user.admin;
    user.saving = true;
    users
        .update({_id: user._id, admin: admin})
        .then(function () {
          user.admin = admin;
          delete user.saving;
        }, function (err) {
          console.error(err);
          delete user.saving;
        });
  }

});
