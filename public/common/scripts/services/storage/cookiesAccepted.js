/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("common").factory("cookiesAccepted", function ($cookies, $q) {
  "use strict";

  var defer = $q.defer();

  /*==================================================== Exports  ====================================================*/

  var service = {
    promise: defer.promise,

    init: init,
    accept: accept,
    reject: function () { defer.reject(); },
    delay: delay
  };

  return service;

  /*=================================================== Functions  ===================================================*/

  function accept(expire) {
    $cookies.put("cookiesAccepted", Date.now() + "", {expires: expire});
    defer.resolve();
  }

  function delay(cb) {
    return function () {
      var args = arguments;
      service.promise.then(function () { cb.apply(null, args); });
    };
  }

  function init() {
    var value = +$cookies.get("cookiesAccepted") > 0 ? true : null;
    if (value === true) { defer.resolve(); }
    return value;
  }

});
