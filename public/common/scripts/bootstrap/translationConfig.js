/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

angular.module("common").config(function (DEFAULT_LOCALE, $translateProvider) {
  "use strict";
  // this is not default implementation, gets customized for delaying until cookies accepted by user
  // see common/scripts/services/storage/$translateLocalStorage.js
  $translateProvider.useLocalStorage();
  $translateProvider.useSanitizeValueStrategy(null);
  $translateProvider.preferredLanguage(DEFAULT_LOCALE);
});
