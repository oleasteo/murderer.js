/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var Q = require("q");

var userC = require.main.require("./core/user/controller");

module.exports = function (queryRoute) {
  queryRoute("exists:username", function (data) {
    if (typeof data !== "string") { return Q.reject("Bad Request."); } // TODO via socket-route-validator TBI
    return userC.qExistsUsername(this, data);
  });

  queryRoute("exists:email", function (data) {
    if (typeof data !== "string") { return Q.reject("Bad Request."); } // TODO via socket-route-validator TBI
    return userC.qExistsEmail(this, data);
  });
};
