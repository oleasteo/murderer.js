/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");
var Q = require("q");
var gameC = require.main.require("./core/game/controller");
var murderC = require.main.require("./core/murder/controller");
var security = require.main.require("./utils/security");

module.exports = function (queryRoute) {
  queryRoute("murder:self", function (data) {
    if (typeof data !== "object" || data == null || typeof data.password !== "string" || typeof data.message !== "string" || typeof data.gameId !== "string" || !data.message.length) {
      return Q.reject("Bad request.");
    }
    if (!security.checkPassword(data.password, this.user.pw)) { return Q.reject("Invalid password."); }
    return gameC.qSuicide(this, this.connection.ip, this.user._id, data.gameId, data.message).then(_.noop);
  });

  queryRoute("murder:token", function (data) {
    if (typeof data !== "object" || data == null || typeof data.token !== "string" || typeof data.message !== "string" || typeof data.gameId !== "string" || typeof data.ringId !== "string" || !data.message.length) {
      return Q.reject("Bad request.");
    }
    var scope = this;
    return gameC
        .qKillByToken(scope, this.connection.ip, this.user._id, data.gameId, data.ringId, data.token, data.message)
        .then(function () { return gameC.qActiveContract(scope, data.gameId, data.ringId); });
  });

  queryRoute("murder:upVote", function (data) {
    if (typeof data !== "string") { return Q.reject("Bad request."); }
    return murderC.qUpVote(this, data);
  });
};
