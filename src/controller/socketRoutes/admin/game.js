/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");

var gameC = require.main.require("./core/game/controller");

module.exports = function (queryRoute) {
  queryRoute("games:admin.all", function (data) { return gameC.qFind(this, data); });

  queryRoute("game:create", function (data) { return gameC.qCreate(this, _.extend(data, {author: this.user._id})); });

  queryRoute("game:lock", function (data) { return gameC.qLock(this, data); });
  queryRoute("game:start", function (data) { return gameC.qStart(this, data); });
  queryRoute("game:resume", function (data) { return gameC.qResume(this, data); });
  queryRoute("game:pause", function (data) { return gameC.qPause(this, data); });
  queryRoute("game:stop", function (data) { return gameC.qStop(this, data); });
  queryRoute("game:remove", function (data) { return gameC.qRemoveSafe(this, data); });
};
