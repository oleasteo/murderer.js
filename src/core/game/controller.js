/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");
var Q = require("q");
var marked = require("marked");

var userC = require.main.require("./core/user/controller");
var murderM = require.main.require("./core/murder/model");

var model = require("./model");
var ctrlBase = require.main.require("./utils/controllerBase");

var contract = require("./services/contract");
var transition = require("./services/transition");
var murder = require("./services/murder");
var participation = require("./services/participation");

/*===================================================== Exports  =====================================================*/

ctrlBase(model, exports);

exports.qGameListEntries = gameListEntries;
exports.qGroupDetails = groupDetails;
exports.qDetails = gameDetails;
exports.qPopulated = findByIdPopulated;

exports.qActiveContract = contract.activeContract;
exports.qActiveContracts = contract.activeContracts;

exports.qKillByToken = murder.byKill;
exports.qSuicide = murder.bySuicide;

exports.qFindJoined = participation.findJoined;
exports.qJoin = participation.join;
exports.qLeave = participation.leave;

exports.qLock = transition.lock;
exports.qStart = transition.start;
exports.qResume = transition.resume;
exports.qPause = transition.pause;
exports.qStop = transition.stop;
exports.qRemoveSafe = transition.remove;

/*==================================================== Functions  ====================================================*/

function gameListEntries(scope) {
  var query = model
      .find(null, {
        "groups.users.message": 0,
        "groups.users.name": 0,
        description: 0,
        "schedule.start": 0,
        "schedule.activate": 0,
        "schedule.deactivate": 0,
        log: 0
      })
      .populate("author", {username: 1, avatarUrl: 1})
      .populate("rings");
  return Q
      .nbind(query.exec, query)()
      .then(function (games) {
        return _.map(games, function (game) {
          return _.extend(game._doc, {
            passwords: !!(game.passwords && game.passwords.length),
            isAlive: isSuicideCommittable(scope.user, game),
            rings: game.started ? game.rings.length : game.startMeta.rings
          });
        });
      });
}

function isSuicideCommittable(user, game) {
  return game.started && userC.isModulePermitted(user, "closed") &&
      _.some(game.rings, function (ring) { return ~murder.getIndexIfSuicideCommittable(user._id, ring); });
}

function groupDetails(ignored, gameId) {
  var query = model
      .findOne({_id: gameId}, {
        "groups.group": 1,
        "groups.users.user": 1,
        "groups.users.name": 1
      })
      .populate("groups.group");
  return Q
      .nbind(query.exec, query)()
      .then(function (game) {
        if (game == null) { return Q.reject("Game not found."); }
        return game.groups;
      });
}

function gameDetails(scope, gameId) {
  var query = model
      .findOne({_id: gameId}, {
        "groups.users.message": 0,
        "schedule.activate": 0,
        "schedule.deactivate": 0,
        log: 0
      })
      .populate("groups.group")
      .populate("author", {username: 1, avatarUrl: 1})
      .populate("rings");
  var murderQuery = murderM // TODO move into murder-controller
      .find({game: gameId}, {ip: 0})
      .populate("trigger", {username: 1, avatarUrl: 1})
      .sort({cdate: 1});
  return Q.spread([
    Q
        .nbind(query.exec, query)()
        .then(function (game) {
          if (game == null) { return Q.reject("Game not found."); }
          game = game._doc;
          game.isAlive = isSuicideCommittable(scope.user, game);
          game.rings = _.map(game.rings, function (ring) {
            return addPresentData({_id: ring._id, active: ring.active}, scope.user, ring);
          });
          game.passwords = !!(game.passwords && game.passwords.length);
          game.description = game.description && marked(game.description);
          return game;
        }),
    Q.nbind(murderQuery.exec, murderQuery)()
  ], function (game, murders) { return {game: game, murders: murders}; });
}

function addPresentData(data, user, ring) {
  if (userC.isModulePermitted(user, "closed")) {
    var chain = ring.chain, userId = user._id, current;
    for (var i = 0; i < chain.length; i++) {
      current = chain[i];
      if (current.user.equals(userId)) {
        data.present = true;
        data.alive = current.murder == null;
        return data;
      }
    }
  }
  data.present = data.alive = false;
  return data;
}

/**
 * Returns Promise for populated game instance by specified ID.
 * @param scope The scope object.
 * @param gameId The ID of the game to fetch.
 * @param [population] The population options (most likely array of paths).
 * @returns Q Promise of populated game instance.
 */
function findByIdPopulated(scope, gameId, population) {
  if (population == null) { population = ["rings"]; }
  scope.log.debug({gameId: gameId, population: population}, "fetching populated game instance");
  var query = model.findById(gameId).populate("rings");
  return Q.nbind(query.exec, query)();
}
