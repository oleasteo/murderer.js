/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var security = require.main.require("./utils/security");

var controller = require("../controller");

var USERNAME_REGEX = /^[a-zA-Z][\w\d]{2,}$/;

controller.validate("create", function (user) {
  if (!USERNAME_REGEX.test(user.username)) { throw new Error("Username invalid."); }

  if (user.password.length < 4) { throw new Error("Password too short."); }

  if (!user.avatarUrl) {
    user.avatarUrl = "//www.gravatar.com/avatar/" + security.md5(user.email.toLowerCase()) + "?d=identicon";
  }

  user.lastName = user.username;
});

controller.validate("remove", function (filter) {
  var user = this.user;
  if (filter == null || !filter.hasOwnProperty("_id") || typeof filter._id !== "string") {
    throw new Error("User remove only allowed by ID");
  }
  var id = filter._id;
  if (id !== user._id && !user.admin) { throw new Error("You are not allowed to remove this user"); }
});

controller.pre("save", function (user) { user.bio = user.bio != null ? user.bio.substring(0, 256) : ""; });
