/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _ = require("lodash");
var fs = require("fs");
var path = require("path");
var express = require("express");

var config = require.main.require("./utils/config").main;
var userC = require.main.require("./core/user/controller");

function sendFile(p, res, cb) {
  p = path.join(config.paths.frontend, p);
  fs.exists(p, function (bool) {
    if (bool) {
      res.sendFile(p);
    } else if (cb !== false && (typeof cb !== "function" || cb(bool) === true)) {
      res.sendStatus(404);
    }
  });
}

function sendPlain(req, res) { sendFile(req.path, res, _.noop); }

function sendFileOrIndex(req, res) {
  req.log.debug({fn: "sendFileOrIndex"});
  sendFile(req.path, res, function (bool) {
    if (!bool) {
      sendIndex(req, res);
    }
  });
}

function getUser(req) {
  return req.isAuthenticated() ? req.user : {guest: true};
}

function sendIndex(req, res) {
  req.log.debug({fn: "sendIndex"});
  var user = getUser(req);
  // iterate over permission-ordered modules to fetch first allowed scope
  _.find(config.modules.all, function (name) {
    if (userC.belongsToModule(user, name)) {
      sendFile("/" + name + ".html", res);
      return true;
    }
  });
}

var dev = config.server.development;

module.exports.pre = function (app) {
  var router = express.Router();

  // setup files that need module-authentication
  _.each(config.modules.all, function (name) {
    router.get("/" + name + ".html", sendIndex);
  });

  var sendScopeFile = function (req, res) {
    var module = req.params.module;
    var user = getUser(req);
    var permitted;
    try { permitted = userC.isModulePermitted(user, module); } catch (err) { return res.sendStatus(404); }
    req.log.debug({fn: "sendScopeFile", permitted: permitted, user: user, module: module});
    if (permitted) {
      sendFile(req.path, res);
    } else {
      res.sendStatus(req.isAuthenticated() ? 403 : 401);
    }
  };

  router.get("/scripts/libs/*", sendPlain);
  router.get("/styles/libs/*", sendPlain);
  router.get("/styles/fonts/*", sendPlain);

  if (dev) {
    router.get("/scripts/:module/*.js", sendScopeFile);
    router.get("/styles/:module/*.css", sendScopeFile);
  } else {
    router.get("/scripts/:module.min.js", sendScopeFile);
    router.get("/styles/:module-*.min.css", sendScopeFile);
  }

  app.use(router);
};

module.exports.post = function (app) {
  var router = express.Router();

  router.get("/plain/cookie-policy", function (req, res) { sendFile("/views/cookiePolicy.html", res, _.noop); });
  router.get("/plain/privacy-policy", function (req, res) { sendFile("/views/privacyPolicy.html", res, _.noop); });
  router.get("/plain/legal-info", function (req, res) { sendFile("/views/legalInfo.html", res, _.noop); });
  router.get("/favicon.ico", sendPlain);
  router.get("/*.*", sendFileOrIndex);
  router.get("/*", sendIndex);

  app.use(router);
};
