/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var compression = require("compression");
var bodyParser = require("body-parser");
var methodOverride = require("method-override");
var cookieParser = require("cookie-parser");
var session = require("express-session");
var bunyan = require("bunyan");
var mongoStore = require("connect-mongo" + (process.version.indexOf("v0.") === 0 ? "/es5" : ""));

var config = require.main.require("./utils/config").main;

module.exports = function (app) {
  // Compression
  app.use(compression({
    threshold: 512 // compress if 512kb or above
  }));

  // Body parser
  //noinspection JSUnresolvedFunction
  app.use(bodyParser.urlencoded({extended: true}));
  //noinspection JSCheckFunctionSignatures
  app.use(bodyParser.json());
  app.use(methodOverride(function (req) {
    if (req.body && typeof req.body === "object" && "_method" in req.body) {
      // look in urlencoded POST bodies and delete it
      //noinspection JSUnresolvedVariable
      var method = req.body._method;
      //noinspection JSUnresolvedVariable
      delete req.body._method;
      return method;
    }
  }));

  // Express/MongoDB session storage
  app.use(cookieParser());
  app.use(session({
    resave: true,
    saveUninitialized: false,
    secret: config.security.secret || config.pkg.name + "-" + config.env,
    cookie: {
      maxAge: config.security.session.expires
    },
    store: mongoStore.create({
      mongoUrl: config.database,
      collectionName: "sessions"
    })
  }));

  var nextId = 0;
  // add logger and id to each request
  app.use(function (req, res, next) {
    req.id = nextId++;
    req.log = bunyan.logger.http.child({req: req, user: req.user}, null, true);
    next();
  });
};
