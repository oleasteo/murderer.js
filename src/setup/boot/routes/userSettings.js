/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var express = require("express");

var userC = require.main.require("./core/user/controller");

function loginAttempt(req, res, successRoute, failStatus) {
  return function (err) {
    if (err == null) {
      res.redirect(successRoute || "/");
    } else {
      req.log.error(err);
      res.sendStatus(failStatus || 500);
    }
  };
}

module.exports = function (app) {
  var router = express.Router();

  router.post("/:username/reset-password/:token", function (req, res) {
    if (!req.body.password || req.body.password !== req.body.repeatPassword) {
      return res.status(400).send(new Error("No password specified or passwords don\"t match."));
    }
    userC
        .qUpdatePasswordByToken(req, req.params.username, req.params.token, req.body.password)
        .then(function (user) { req.logIn(user, loginAttempt(req, res)); }, function (err) {
          if (typeof err === "string") {
            req.log.error({message: err}, "password reset failed");
            res.redirect("/401");
          } else {
            req.log.error({err: err});
            res.send(err);
          }
        });
  });

  app.use("/settings", router);
};
