/* Copyright (C) 2016  Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var passport = require("passport");
var passportLocal = require("passport-local");

var security = require.main.require("./utils/security");
var userModel = require.main.require("./core/user/model");

module.exports = function (app) {

  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser(function (user, cb) {
    cb(null, user._id.toHexString());
  });

  passport.deserializeUser(userModel.findById);

  passport.use(new passportLocal.Strategy(function (username, password, cb) {
    userModel.findByUsername(username, function (err, user) {
      if (err != null) {
        cb(err);
      } else if (user == null || !security.checkPassword(password, user.pw)) {
        cb(null, false, {message: "validation.login.invalid"});
      } else {
        cb(null, user);
      }
    });
  }));
};
