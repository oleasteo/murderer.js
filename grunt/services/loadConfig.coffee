###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'
path = require 'path'

module.exports = (grunt, cwd, configDir, configObj = {}) ->
  ###---------------------------------------------- Load config files  ----------------------------------------------###
  _.each ['init', 'modules', 'paths', 'locales', 'styles', 'provide', 'libraries', 'copy'], (key) ->
    file = path.join configDir, key + ".json"
    fileCustom = path.join configDir, key + ".local.json"
    configObj[key] = content = if grunt.file.exists file then grunt.file.readJSON file else {}
    _.merge content, grunt.file.readJSON fileCustom if grunt.file.exists fileCustom

  ###----------------------------------------------- Add package-info -----------------------------------------------###
  configObj.package = grunt.file.readJSON path.join cwd, 'package.json'

  ###--------------------------------------------- Add bower target-dir ---------------------------------------------###
  bowerCfg = path.join cwd, '.bowerrc'
  bower = configObj.paths.bower =
    relative: grunt.file.exists(bowerCfg) && (grunt.file.readJSON bowerCfg).directory || 'bower_components'
    destination:
      dev: path.join configObj.paths.destination.dev, configObj.paths.destinationLibs
      dist: path.join configObj.paths.destination.dist, configObj.paths.destinationLibs
  bower.absolute = path.join cwd, bower.relative

  ###-------------------------------------------- Prepare init for tasks --------------------------------------------###
  configObj._init = {}

  addStaticTemplates = (mod, name) ->
    _.each grunt.file.expand("#{configObj.paths.source}/#{name}/templates-static/*.html"), (file) ->
      key = /([^\/]+)\.html$/.exec(file)[1]
      if !configObj._init.hasOwnProperty key
        configObj._init[key] =
          key: key
          camelKey: key.replace(/_+(.)/, (ignored, char) -> char.toUpperCase())
          modules: [name]
          fileFn: (n) -> "#{configObj.paths.source}/#{n}/templates-static/#{key}.html"
      else
        configObj._init[key].modules.push name

  _.each configObj.modules, addStaticTemplates

  ###----------------------------------------- Prepare Libraries for tasks  -----------------------------------------###

  configObj.__libs =
    js: _.mapValues(configObj.libraries.javascript, (libs) -> _.keys(libs))
    jsMin: _.mapValues(configObj.libraries.javascript, (libs) ->
      _(libs).map((minJs, js) ->
        if typeof minJs == 'boolean' then js.replace(/(\.[^\.]+)$/, ".min$1") else minJs
      ).flatten().value()
    )
    static: _.mapValues(configObj.libraries.files, (libs) -> _.map libs, (lib) ->
      _.extend {expand: true}, lib, {cwd: path.join bower.relative, lib.cwd}
    )
    css: _.mapValues(configObj.libraries.less, (libs) ->
      obj = {}
      _.each libs, (lib) ->
        obj[lib.media] = [] if !obj[lib.media]?
        obj[lib.media].push "/styles/libs/#{lib.id}.css"
      obj
    )
    cssMin: _.mapValues(configObj.libraries.less, (libs) ->
      obj = {}
      _.each libs, (lib) ->
        obj[lib.media] = [] if !obj[lib.media]?
        obj[lib.media].push "/styles/libs/#{lib.id}.min.css"
      obj
    )

  configObj.__scripts =
    js: _(configObj.modules).mapValues((module, name) -> ["scripts/#{name}/main.js", "scripts/#{name}/**/*.js"]).value()
    jsMin: _(configObj.modules).mapValues((module, name) -> ["scripts/#{name}.min.js"]).value()

  configObj.__styles =
    css: _(configObj.modules).mapValues((module, name) ->
      fPath = "#{configObj.paths.source}/#{name}/styles"
      _.mapValues configObj.styles, (keys) -> _.compact _.map keys, (key) ->
        if grunt.file.exists path.join fPath, "#{key}.less" then "/styles/#{name}/#{key}.css" else null
    ).value()
    cssMin: _(configObj.modules).mapValues((module, name) ->
      fPath = "#{configObj.paths.source}/#{name}/styles"
      _.mapValues configObj.styles, (keys) -> _.compact _.map keys, (key) ->
        if grunt.file.exists path.join fPath, "#{key}.less" then "/styles/#{name}-#{key}.min.css" else null
    ).value()

  ###---------------------------------------------- Attach some values ----------------------------------------------###

  configObj.cwd = cwd;

  configObj.loadServerConfig = (name, env) ->
    res = grunt.file.readJSON path.join cwd, "config/#{name}.json"
    if grunt.file.exists path.join cwd, "config/#{name}.local.json"
      _.merge res, grunt.file.readJSON path.join cwd, "config/#{name}.local.json"
    if res._noEnv
      delete res._noEnv
      res
    else
      c = res[env]
      while c._extend
        k = c._extend
        delete c._extend
        c = _.extend {}, res[k], c
      c

  configObj
