###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

module.exports = (config, grunt) ->
  grunt.loadNpmTasks 'grunt-contrib-clean'

  paths = config.paths

  getDestinations = (suffix) ->
    [
      paths.destination.dist + suffix
      paths.destination.dev + suffix
    ]

  clean =
    all: getDestinations ''
    html: getDestinations '/*.html'
    styles: getDestinations '/styles'
    scripts: getDestinations '/scripts'
    translations: getDestinations '/scripts/*/configs/translations.js'

  clean