###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'

module.exports = (config, grunt) ->
  paths = config.paths

  getContent = (name, id, data) -> "angular.module(\"#{name}\").constant(\"#{id}\", #{JSON.stringify data});"

  writeFile = (getDestination, name, id, data) ->
    destination = getDestination name, id
    grunt.file.write destination, getContent name, id, data

  grunt.registerMultiTask "genConstants", "Adds angular-constants as defined within provide config.", ->
    data = this.data
    _.each data.constants, (value) -> writeFile data.getDestination, value.module, value.constant, value.data

  all:
    constants: config.provide.angular.constants
    getDestination: (module, id) -> "#{paths.destination.dev}/scripts/#{module}/constants/#{id}.js"
