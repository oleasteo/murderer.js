###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'
path = require 'path'

module.exports = (config, grunt, helpers) ->
  grunt.loadNpmTasks 'grunt-contrib-copy'

  paths = config.paths
  copy =
    dev_static:
      files: [
        expand: true
        src: "**/*"
        cwd: path.join paths.source, 'static'
        dest: paths.destination.dev
      ]
    dist_static:
      files: [
        expand: true
        src: "**/*"
        cwd: path.join paths.source, 'static'
        dest: paths.destination.dist
      ]
    dist_libs:
      files: [
        expand: true
        cwd: paths.bower.relative
        dest: paths.bower.destination.dist
        src: _(config.__libs.jsMin).values().flatten().value()
      ]
    dev_libs:
      files: [
        expand: true
        cwd: paths.bower.relative
        dest: paths.bower.destination.dev
        src: _(config.__libs.js).values().flatten().value()
      ]

  getStaticOptions = (name, obj, destination) ->
    cwd: path.join paths.source, name, obj
    dest: path.join destination, obj, name
    src: '**/*'
    expand: true

  _.each config.modules, (mod, name) ->
    recursiveFiles = helpers.recursiveFilesBuilder name, mod, true

    devLibsFiles = recursiveFiles (n) ->
      _.map(config.__libs.static[n], (lib) ->
        lib = _.clone lib
        lib.dest = if lib.dest? then "#{paths.destination.dev}/#{lib.dest}" else paths.destination.dev
        lib
      ).concat _.map(config.copy.static, (obj) -> getStaticOptions name, obj, paths.destination.dev)

    distLibsFiles = recursiveFiles (n) ->
      _.compact(_.map(config.__libs.static[n], (lib) ->
        return null if !lib.deploy
        lib = _.clone lib
        lib.dest = if lib.dest? then "#{paths.destination.dist}/#{lib.dest}" else paths.destination.dist
        lib
      )).concat _.map(config.copy.static, (obj) -> getStaticOptions name, obj, paths.destination.dist)

    copy[name + '_dev_js'] =
      files: [
        expand: true
        src: recursiveFiles (n) -> "#{n}/scripts/**/*.js"
        cwd: paths.source
        dest: paths.destination.dev
        rename: (dest, path) -> "#{dest}/#{path.replace /^([^\/]*)\/scripts/, 'scripts/$1'}"
      ]

    copy[name + '_dev_static'] =
      files: devLibsFiles

    copy[name + '_dist_static'] =
      files: distLibsFiles

    return if !mod.deploy

    copy[name + '_dev_less'] =
      files: [
        expand: true
        src: recursiveFiles (n) -> "#{n}/styles/**/*.less"
        cwd: paths.source
        dest: paths.destination.dev
        rename: (dest, path) -> "#{dest}/#{path.replace /^([^\/]*)\/styles/, 'styles/$1'}"
      ]

  helpers.registerTaskByPattern 'copy_dev', 'copy', copy, /(?:^|_)dev(?:_|$)/
  helpers.registerTaskByPattern 'copy_dist', 'copy', copy, /(?:^|_)dist(?:_|$)/

  helpers.registerTaskByPattern 'copy_dev_js', 'copy', copy, /(?:^|_)dev_js/
  helpers.registerTaskByPattern 'copy_dev_less', 'copy', copy, /(?:^|_)dev_less/
  helpers.registerTaskByPattern 'copy_dev_static', 'copy', copy, /(?:^|_)dev_static/
  helpers.registerTaskByPattern 'copy_dist_static', 'copy', copy, /(?:^|_)dist_static/

  copy