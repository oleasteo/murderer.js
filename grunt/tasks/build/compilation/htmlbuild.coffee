###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'
path = require 'path'

module.exports = (config, grunt, helpers) ->
  grunt.loadNpmTasks 'grunt-html-build'

  paths = config.paths

  staticTemplates = {}

  _.each config._init, (tpl) ->
    staticTemplates[tpl.camelKey] = tpl.fileFn if !staticTemplates.hasOwnProperty tpl.camelKey

  getSections = (n) ->
    obj = {}
    for key, pathFn of staticTemplates
      file = helpers.findFile n, pathFn
      obj[key] = if file? then file else "grunt/helper/empty_file"
    obj

  getStyles = (styles, mod, name, wrap) ->
    prefix = ""
    if mod.dependencies?.length
      _.each mod.dependencies, (dependency) -> prefix += getStyles styles, config.modules[dependency], dependency, wrap
    wrapMulti = (files, media) -> if !files.length then null else _.map(files, _.partial(wrap, _, media)).join("\n")
    prefix += _.flatten(_.compact(_.map styles[name], wrapMulti)).join "\n"
    prefix += "\n" if prefix && !prefix.endsWith "\n"
    prefix

  wrapStyle = (file, media) -> "<link type='text/css' rel='stylesheet' media='#{media}' href='#{file}'>"

  prefixBowerDist = (lib) -> path.join paths.bower.destination.dist, lib
  prefixBowerDev = (lib) -> path.join paths.bower.destination.dev, lib
  prefixDev = (lib) -> path.join paths.destination.dev, lib
  prefixDist = (lib) -> path.join paths.destination.dist, lib

  htmlbuild = {}

  _.each config.modules, (mod, name) ->
    return if !mod.deploy

    sections = getSections name

    fileBuilder = helpers.recursiveFilesBuilder name, mod, true

    libsDev = _.map fileBuilder((n) -> config.__libs.js[n] || []), prefixBowerDev
    libsDist = _.map fileBuilder((n) -> config.__libs.jsMin[n] || []), prefixBowerDist

    stylesDev = getStyles config.__libs.css, mod, name, wrapStyle
    stylesDev += getStyles config.__styles.css, mod, name, wrapStyle
    stylesDev = stylesDev.substring 0, stylesDev.length - 1
    stylesDist = getStyles config.__libs.cssMin, mod, name, wrapStyle
    stylesDist += getStyles config.__styles.cssMin, mod, name, wrapStyle
    stylesDist = stylesDist.substring 0, stylesDist.length - 1

    modulesDev = _.map fileBuilder((n) -> config.__scripts.js[n] || []), prefixDev
    modulesDist = _.map fileBuilder((n) -> config.__scripts.jsMin[n] || []), prefixDist

    htmlbuild[name + '_dev'] =
      src: helpers.findFile(name, (n) -> "#{paths.source}/#{n}/index.html")
      dest: "#{paths.destination.dev}/#{name}.html"
      options:
        prefix: '/'
        relative: true
        scripts:
          libs: libsDev
          modules: modulesDev
        sections: sections
        data:
          base: config.loadServerConfig("server", "dev").url
          title: "#{config.package.name} - Development - #{config.package.version}"
          app: name
          styles: stylesDev

    htmlbuild[name + '_dist'] =
      src: helpers.findFile(name, (n) -> "#{paths.source}/#{n}/index.html")
      dest: "#{paths.destination.dist}/#{name}.html"
      options:
        prefix: '/'
        relative: true
        scripts:
          libs: libsDist
          modules: modulesDist
        sections: sections
        data:
          base: config.loadServerConfig("server", "dist").url
          title: config.package.name
          app: name
          styles: stylesDist

  helpers.registerTaskByPattern 'htmlbuild_dev', 'htmlbuild', htmlbuild, /_dev/
  helpers.registerTaskByPattern 'htmlbuild_dist', 'htmlbuild', htmlbuild, /_dist/

  htmlbuild