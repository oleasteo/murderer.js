###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'
path = require 'path'

module.exports = (config, grunt, helpers) ->
  grunt.loadNpmTasks "grunt-contrib-less"

  paths = config.paths

  less =
    options:
      paths: [paths.destination.dev + '/styles']
      ieCompat: false
      relativeUrls: false

  _.each config.modules, (mod, name) ->

    for lib in config.libraries.less[name] || []
      less[name + '_dev_lib_' + lib.id] =
        files: [
          src: path.join paths.bower.relative, lib.cwd, lib.src
          dest: "#{paths.destination.dev}/styles/libs/#{lib.id}.css"
        ]
        options:
          paths: [path.join paths.bower.relative, lib.cwd]
          sourceMap: true
          sourceMapURL: "/styles/libs/#{lib.id}.css.map"
          sourceMapBasepath: path.join paths.bower.relative, lib.cwd
          sourceMapRootpath: "/#{lib.sources}"
      less[name + '_dist_lib_' + lib.id] =
        files: [
          src: path.join paths.bower.relative, lib.cwd, lib.src
          dest: "#{paths.destination.dist}/styles/libs/#{lib.id}.min.css"
        ]
        options:
          paths: [path.join paths.bower.relative, lib.cwd]

    for ignored, keys of config.styles
      for key in keys when grunt.file.exists "#{paths.source}/#{name}/styles/#{key}.less"
        less[name + '_dev_' + key] =
          files: [
            src: "#{paths.source}/#{name}/styles/#{key}.less"
            dest: "#{paths.destination.dev}/styles/#{name}/#{key}.css"
          ]
          options:
            paths: [paths.destination.dev + '/styles']
            sourceMap: true
            sourceMapURL: "/styles/#{name}/#{key}.css.map"
            sourceMapBasepath: path.join paths.source, name, "styles"
            sourceMapRootpath: "/styles/#{name}"

    less[name + '_dist'] =
      files: _(config.styles).map((keys) -> _.compact(_.map(keys, (key) ->
        if grunt.file.exists "#{paths.source}/#{name}/styles/#{key}.less"
          return {
            src: "#{paths.source}/#{name}/styles/#{key}.less"
            dest: "#{paths.destination.dist}/styles/#{name}-#{key}.min.css"
          }
      ))).flatten().value()
      options:
        paths: [paths.source, paths.destination.dev + '/styles']

  helpers.registerTaskByPattern 'less_dev', 'less', less, /_dev/
  helpers.registerTaskByPattern 'less_dist', 'less', less, /_dist/

  less