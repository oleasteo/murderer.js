###
# Copyright (C) 2016  Ole Reglitzki
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
###

_ = require 'lodash'
path = require 'path'

SOME_COMMENTS = /(?:^!|@(?:license|preserve|cc_on))/

module.exports = (config, grunt) ->
  grunt.loadNpmTasks 'grunt-contrib-uglify'

  paths = config.paths
  prefixBowerDist = (lib) -> path.join paths.bower.destination.dist, lib
  prefixBower = (lib) -> path.join paths.bower.relative, lib
  prefixDev = (lib) -> path.join paths.destination.dev, lib

  uglify = {}

  getBanner = (name) ->
    """
    /*!
     * <%= pkg.name %> v<%= pkg.version %> (https://github.com/frissdiegurke/murderer.js)
     * Copyright (C) <%= grunt.template.today('yyyy') %>  Ole Reglitzki
     * Build: <%= grunt.template.today('yyyy-mm-dd') %>
     * Module: #{name}
     * License: GPL-3.0, see <http://www.gnu.org/licenses/>
     */\n
    """

  uglify.libs =
    options:
      preserveComments: SOME_COMMENTS
    files: {}

  _.each config.libraries.javascript, (libs) ->
    _.compact _.map libs, (minJs, js) -> if minJs == false
      uglify.libs.files[prefixBowerDist js.replace /(\.[^\.]+)$/, ".min$1"] = prefixBower js

  _.each config.modules, (mod, name) ->
    uglify[name] =
      options:
        banner: getBanner name
        preserveComments: SOME_COMMENTS
      files: {}
    uglify[name].files["#{paths.destination.dist}/scripts/#{name}.min.js"] = config.__scripts.js[name].map prefixDev

  uglify